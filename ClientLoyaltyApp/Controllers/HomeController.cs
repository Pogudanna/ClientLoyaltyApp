﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientLoyaltyApp.Models;

namespace ClientLoyaltyApp.Controllers
{
    public class HomeController : Controller
    {
        private ClientContext context = new ClientContext();

        public ActionResult Index()
        {
            //string s = @"C:\Users\*\Рабочий стол\1.txt";
            //using (var sr = new StreamReader(s))
            //{
                //context.Clients.Add(new Client()
                //{
                //    Id = 1,
                //    Genger = "M",
                //    //1	M	48	MAR	UMN	59998	10	01.июн	0,770249	GRD	КРАСНОДАРСКИЙ КРАЙ	30000	1	1	0

                //    Age = 48,
                //    MartialStatus = "MAR",
                //    JobPosition = "UMN",
                //    CreditSum = 59998,
                //    CreditMonth = 10,
                //    Education = "GRD",
                //    LivingRegion = "КРАСНОДАРСКИЙ КРАЙ",
                //    MonthlyIncome = 30000,
                //    CreditCount = 1,
                //    OverdueCreditCount = 1,
                //    OpenAccountFlag = false
                //});
                //context.SaveChanges();
            //    var str = sr.ReadLine();
            //    while (!string.IsNullOrEmpty(str))
            //    {
            //        var arr = str.Split('\t');
            //        context.Clients.Add(new Client()
            //        {
            //            Id = Convert.ToInt32(arr[0]),
            //            Genger = arr[1],
            //            Age = Convert.ToInt32(arr[2]),
            //            MartialStatus = arr[3],
            //            JobPosition = arr[4],
            //            CreditSum = Convert.ToDouble(arr[5]),
            //            CreditMonth = Convert.ToInt32(arr[6]),
            //            Education = arr[7],
            //            LivingRegion = arr[8],
            //            MonthlyIncome = arr[9] != "" ? Convert.ToDouble(arr[9]) : 0,
            //            CreditCount = arr[10] != "" ? Convert.ToInt32(arr[10]) : 0,
            //            OverdueCreditCount = arr[11] != "" ? Convert.ToInt32(arr[11]) : 0,
            //            OpenAccountFlag = arr[12] == "0" ? false : true
            //        });
            //        str = sr.ReadLine();
            //    }
            //}
            //context.SaveChanges();
            return View(context.Clients.ToList());
            
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Client client)
        {
            context.Entry(client).State = EntityState.Added;
            context.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]

        public ActionResult EditClient(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Client client = context.Clients.Find(id);
            if (client != null)
            {
                return View(client);
            }
            return HttpNotFound();
        }



        [HttpPost]
        public ActionResult EditClient(Client client)
        {
            context.Entry(client).State = EntityState.Modified;
            context.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            Client client = context.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = context.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            context.Clients.Remove(client);
            context.SaveChanges();
            return RedirectToAction("Index");

        }
    }
}