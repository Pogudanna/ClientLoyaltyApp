﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClientLoyaltyApp.Models
{
    public class ClientContext:DbContext
    {
        public ClientContext()
        { }

        public DbSet<Client> Clients { get; set; }
    }
}
