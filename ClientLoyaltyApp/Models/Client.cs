﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientLoyaltyApp.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string Genger { get; set; }
        public int Age { get; set; }
        public string MartialStatus { get; set; }
        public string JobPosition { get; set; }
        public double CreditSum { get; set; }
        public int CreditMonth { get; set; }
        public string Education { get; set; }
        public string LivingRegion { get; set; }
        public double MonthlyIncome { get; set; }
        public int CreditCount { get; set; }
        public int OverdueCreditCount { get; set; }
        public bool OpenAccountFlag { get; set; }
    }
}